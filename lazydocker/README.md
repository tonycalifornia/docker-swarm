## LazyDocker Notes

### Usage

#### For x86 based systems

Use the lazyteam image:

```
$ docker run --rm -it -v \
/var/run/docker.sock:/var/run/docker.sock \
-v config:/.config/jesseduffield/lazydocker \
--name lazy-docker lazyteam/lazydocker
```

#### For ARM based systems

Use custom built image:

```
$ docker run --rm -it -v \
/var/run/docker.sock:/var/run/docker.sock \
-v config:/.config/anthonyrussano/lazydockerarm \
--name lazy-docker anthonyrussano/lazydockerarm
```
